const SubCategoryType = require('../models/subCategoryType');
const { errorHandler } = require('../helpers/dbErrorHandler');
const { isValidObjectId } = require('mongoose');

exports.subCategoryTypeById = (req, res, next, id) => {
    SubCategoryType.findById(id)
    .populate("products").exec((err, data) => {
        if (err || !data) {
            return res.status(400).json({
                error: "Sub Category is not Exist"
            })
        }
        req.subcategorytype = data
        next();
    })
}

exports.createSubcategoryType = (req, res) => {
    try {
        const {
            name,
            subcategoryId
        } = req.body


        if (!name || !subcategoryId) {
            throw {
                message: "Name & SubCategory is required"
            }
        }

        if (!isValidObjectId(subcategoryId)) {
            throw {
                message: "SubCategory is not valid",
            }
        }

        let subCategorytype = new SubCategoryType(req.body);
        subCategorytype.save((err, result) => {
            if (err) {
                return res.status(400).json({
                    error: errorHandler(err)
                })
            }

            res.json(result)
        })
    } catch (error) {
        return res.status(400).json({
            error: error.message ? error.message : "Server error"
        })
    }
};


exports.listSubCategoryTypes = (req, res) => {

    SubCategoryType.find({subcategoryId:req.subcategory._id})
    .populate("products").exec((err, data) => {
            if (err) {
                return res.status(400).json({
                    error: errorHandler(err)
                })
            }
            res.json({
                subcategory:req.subcategory,
                subcategoryTypes:data
            });
        })

}

exports.readSubCategoryType = (req, res) => {
    return res.json(req.subcategorytype);
}
