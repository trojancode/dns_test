const SubCategory = require('../models/subcategory');
const { errorHandler } = require('../helpers/dbErrorHandler');
const { isValidObjectId } = require('mongoose');

exports.subCategoryById = (req, res, next, id) => {
    SubCategory.findById(id)
    .populate("products")
    .exec((err, data) => {
        if (err || !data) {
            return res.status(400).json({
                error: "Category is not Exist"
            })
        }
        req.subcategory = data
        next();
    })
}

exports.createSubcategory = (req, res) => {
    try {
        const {
            name,
            categoryId
        } = req.body


        if (!name || !categoryId) {
            throw {
                message: "Name & category is required"
            }
        }

        if (!isValidObjectId(categoryId)) {
            throw {
                message: "Category is not valid",
            }
        }

        let subCategory = new SubCategory(req.body);
        subCategory.save((err, result) => {
            if (err) {
                return res.status(400).json({
                    error: errorHandler(err)
                })
            }

            res.json(result)
        })
    } catch (error) {
        return res.status(400).json({
            error: error.message ? error.message : "Server error"
        })
    }
};


exports.listSubCategory = (req, res) => {

    SubCategory.find({categoryId:req.category._id})
    .populate("products").exec((err, data) => {
        if (err) {
            return res.status(400).json({
                error: errorHandler(err)
            })
        }
        res.json({
            category:req.category,
            subcategories:data
        });
    })

}

exports.readSubCategory = (req, res) => {
    return res.json(req.subcategory);
}
