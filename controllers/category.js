const Category = require('../models/category');
const { errorHandler } = require('../helpers/dbErrorHandler');

exports.categoryById = async (req, res, next, id) => {
    await Category.findById(id)
        .populate('products')
        .exec((err, category) => {
            if (err || !category) {
                return res.status(400).json({
                    error: "Category is not Exist"
                })
            }
            req.category = category
            next();
        })
}

exports.create = (req, res) => {
    //check for all fields
    try {
        const {
            name
        } = req.body

        if (!name) {
            throw {
                message: "Name is required"
            }
        }

        let category = new Category(req.body);
        category.save((err, result) => {
            if (err) {
                return res.status(400).json({
                    error: err.errmsg,
                })
            }

            res.json(result)
        })
    } catch (error) {
        return res.status(400).json({
            error: error.message ? error.message : "Server error"
        })
    }
};



exports.read = async (req, res) => {
    return res.json(req.category);
}

exports.list = (req, res) => {

    Category.find()
    .populate("products").exec((err, data) => {
        if (err) {
            return res.status(400).json({
                error: errorHandler(err)
            })
        }

        res.json(data);
    })

}