const Product = require('../models/product');
const { errorHandler } = require('../helpers/dbErrorHandler');
const category = require('../models/category');
const subcategory = require('../models/subcategory');
const subCategoryType = require('../models/subCategoryType');

exports.productById = async (req, res, next, id) => {
    await Product.findById(id)
        .exec((err, data) => {
            if (err || !data) {
                return res.status(400).json({
                    error: "Category is not Exist"
                })
            }
            req.product = data
            next();
        })
}

exports.createProduct =async (req, res) => {
    //check for all fields
    try {
        const {
            name,
            price,
            categoryId,
            subcategoryId,
            subcategoryTypeId
        } = req.body

        if (!name || !price || !categoryId || !subcategoryId || !subcategoryTypeId) {
            throw {
                message: "All fields are required"
            }
        }

        let product = new Product(req.body);
        await product.save((err, result) => {
            if (err) {
                return res.status(400).json({
                    error: err.errmsg,
                })
            }
        })
        await category.updateOne({_id:categoryId},{
            $push :{
                products:product._id
            }
        })
        await subcategory.updateOne({_id:subcategoryId},{
            $push :{
                products:product._id
            }
        })
        await subCategoryType.updateOne({_id:subcategoryTypeId},{
            $push :{
                products:product._id
            }
        })

        return res.json(product)

    } catch (error) {
        return res.status(400).json({
            error: error.message ? error.message : "Server error"
        })
    }
};



exports.read = async (req, res) => {
    return res.json(req.category);
}

exports.listBySubcategoryType = (req, res) => {

    Product.find({subcategoryTypeId:req.subcategorytype._id}).exec((err, data) => {
        if (err) {
            return res.status(400).json({
                error: errorHandler(err)
            })
        }

        res.json({
            subcategoryType:req.subcategorytype,
            products:data
        });
    })

}