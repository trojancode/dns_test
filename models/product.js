const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: true,
        maxlength: 32,
        unique: true
    },
    price:{
        type:Number
    },
    categoryId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Category"
    },
    subcategoryId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "SubCategory"
    },
    subcategoryTypeId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "SubCategoryType"
    },
}, {
    timestamps: true
});


module.exports = mongoose.model("Product", productSchema);