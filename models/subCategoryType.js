const mongoose = require('mongoose');

const subCategoryTypeSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: true,
        maxlength: 32,
        unique: true
    },
    subcategoryId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "SubCategory"
    },
    products: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Product"
    }],
}, {
    timestamps: true
});


module.exports = mongoose.model("SubCategoryType", subCategoryTypeSchema);