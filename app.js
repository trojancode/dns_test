const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const expressValidator = require('express-validator');
require('dotenv').config();

const categoryRoutes = require('./routes/category')
const subcategoryRoutes = require('./routes/subcategoryRoutes')
const subcategoryTypeRoutes = require('./routes/subcategorySubTypesRoutes')
const productRoutes = require('./routes/productRoutes')


//app
const app = express();

//db
mongoose.connect(process.env.DATABASE, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true
    }).then(() => console.log("DB Connected"))
    .catch((ero) => console.log(ero));

//midddleware
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(expressValidator());
app.use(cors());


 
//routes middlewares
app.use('/api/category',categoryRoutes);
app.use('/api/subcategory',subcategoryRoutes);
app.use('/api/subcategorytype',subcategoryTypeRoutes);
app.use('/api/product',productRoutes);



const port = process.env.PORT || 8000;

app.listen(port, () => {
    console.log(`server runnig on por:${port}`);
})