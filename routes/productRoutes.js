const express = require('express');
const { createProduct,productById, listBySubcategoryType } = require('../controllers/product');
const { subCategoryTypeById } = require('../controllers/subcategorySubTypes');
const router = express.Router();




router.post('/create', createProduct);
router.get('/bysubcategoryType/:subcategorytypeId', listBySubcategoryType)

router.param("productId", productById);
router.param("subcategorytypeId", subCategoryTypeById);


module.exports = router;