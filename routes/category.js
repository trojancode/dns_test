const express = require('express');
const router = express.Router();


const { create, photo, categoryById, read, update, remove, list } = require('../controllers/category');


router.get('/:categoryId', read)
router.post('/create', create);
router.get('/', list)

router.param("categoryId", categoryById);


module.exports = router;