const express = require('express');
const { subCategoryById } = require('../controllers/subcategory');
const { createSubcategoryType, listSubCategoryTypes, readSubCategoryType, subCategoryTypeById } = require('../controllers/subcategorySubTypes');
const subCategoryType = require('../models/subCategoryType');
const router = express.Router();


router.post('/create/', createSubcategoryType);
router.get('/:subcategorytypeId', readSubCategoryType);
router.get('/bySubCategory/:subcategoryId', listSubCategoryTypes)

router.param("subcategoryId", subCategoryById);
router.param("subcategorytypeId", subCategoryTypeById);


module.exports = router;