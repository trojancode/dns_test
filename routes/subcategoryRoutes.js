const express = require('express');
const { createSubcategory, listSubCategory, subCategoryById, readSubCategory } = require('../controllers/subcategory');
const { categoryById } = require('../controllers/category');

const router = express.Router();




// router.get('/subcategory/:categoryId', read)
router.post('/create/', createSubcategory);
router.get('/:subcategoryId', readSubCategory)

router.get('/byCategory/:categoryId', listSubCategory)
router.param("categoryId", categoryById);

router.param("subcategoryId", subCategoryById);


module.exports = router;